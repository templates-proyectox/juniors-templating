# Famework skeleton for Junior Teams based on Slim Skeleton 

## Intention
This framework was created in focus of having an stable MVC structure based on Slim-Skeleton.

## Features
- Twig templates
- Load environment files using vlucas/phpdotenv
- Use Slim PHP v3
- Support migrations/seeders using phinx 
- Support for abtraction layer for database using Eloquent illuminate/database
- Monolog for Logging
- CORS Middleware activated by uupola/cors-middleware

## Create a new project
We only need to create a new project using **composer** 

```bash
composer create-project proyect-x/junior-team-framework [directory_name]
```

## Project setup
- The instalation created an environment file named **.env**, the file contains the following parameters for configure our system:
    - **PROJECT_NAME:** name of our application, it's informative
    - **SERVER_PORT:** port for expose our system, this variable is used by **docker-compose** and **composer start**
    - **ENV:** environment for our application, Support the values DEV and PROD, if we put **PROD**, Twig will use cache for the templates and the errors will not be displayed.
    - **DEBUG_LEVEL:** Level for logging, the level supported are **DEBUG**, **INFO**, **NOTICE**, **WARNING**, **ERROR** and **CRITICAL**.
    - **DB_DRIVER:** driver for eloquent and phinx migrations, the drivers supported are MySQL, PostgreSQL, SQLite, SQL Server, please check the [Eloquent](https://laravel.com/docs/7.x/database#configuration) and [Phinx](https://book.cakephp.org/phinx/0/en/configuration.html) documentation.
    - **DB_HOSTNAME:** hostname or ip for the connection to the database (this parameter is used by **docker-compose** for create a link for the mysql container).
    - **DB_DATABASE:** database name, if you choose to use **composer start** you need to create the database manually, if you choose **docker-compose** the mysql container will create the database for you.
    - **DB_USERNAME:** username from the database 
    - **DB_PASSWORD:** password from the database
    - **DB_PORT:** port from the database
    - **DB_CHARSET:** charset from the database
    - **DB_COLLATION:** collation from the database
    - **TIMEZONE:** timezone for the database and php containers.

## Turning up the project
### Using composer
We need to configure the database, after that we need to type the following command:

```bash
composer start
```

And our system will be displayed in the port specified in the **.env** file **SERVER_PORT**

### Using docker-compose
We need to type the following command:

```bash
docker-compose up
```

And our system will be displayed in the port specified in the **.env** file **SERVER_PORT**, docker compose will create two containers, the first is a container with nginx and the second a mysql server.


