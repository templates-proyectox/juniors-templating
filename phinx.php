<?php
require 'vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createUnsafeMutable(__DIR__);
$dotenv->load();

return [
    "paths" => [
        "migrations" => __DIR__."/migrations",
        "seeds" => __DIR__."/seeders"
    ],
    "seed_base_class" => "Modules\Utils\Core\Seeder",
    "migration_base_class" => "Modules\Utils\Core\Migration",
    "environments" => [
        "default_migration_table" => "migrations",
        "default_database" => "production",
        "production" => [
            "adapter" => $_ENV['DB_DRIVER'],
            "host" => $_ENV["DB_HOSTNAME"],
            "name" => $_ENV["DB_DATABASE"],
            "user" => $_ENV["DB_USERNAME"],
            "pass" => $_ENV["DB_PASSWORD"],
            "port" => $_ENV["DB_PORT"],
            "charset" => $_ENV["DB_CHARSET"] ?? "utf8",
            "collation" => $_ENV["DB_COLLATION"] ?? "utf8_unicode_ci",
        ]
    ],
    "version_order" => "creation"
];
