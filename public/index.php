<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

use Modules\Utils\Core\BootMigrationConfiguration;
use Slim\App;

session_start();

$dotenv = Dotenv\Dotenv::createUnsafeMutable(__DIR__."/../");
$dotenv->load();

// Instantiate the app
$settings = require __DIR__ . '/../core/settings.php';
$app = new App($settings);

// Set up dependencies
$dependencies = require __DIR__ . '/../core/dependencies.php';
$dependencies($app);

// Register middleware
$middleware = require __DIR__ . '/../core/middleware.php';
$middleware($app);

// Register routes
$routes = require __DIR__ . '/../core/routes.php';
$routes($app);

BootMigrationConfiguration::boot();
// Run app
$app->run();
