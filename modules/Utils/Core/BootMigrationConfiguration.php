<?php
namespace Modules\Utils\Core;

use Illuminate\Database\Capsule\Manager;

class BootMigrationConfiguration
{
    public static function boot()
    {
        $manager = new Manager();

        $manager->addConnection([
            'driver' => $_ENV['DB_DRIVER'],
            'host' => $_ENV['DB_HOSTNAME'],
            'database' => $_ENV['DB_DATABASE'],
            'username' => $_ENV['DB_USERNAME'],
            'password' => $_ENV['DB_PASSWORD'],
            'charset' => $_ENV['DB_CHARSET'] ?? 'utf8',
            'collation' => $_ENV['DB_COLLATION']
        ]);

        $manager->setAsGlobal();
        $manager->bootEloquent();
        $schema = $manager->schema();
        return [$manager,$schema];
    }
}
