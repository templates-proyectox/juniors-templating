<?php
namespace Modules\Utils\Core;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigCoreExtensions extends AbstractExtension
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getName()
    {
        return 'core';
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('get_session', [$this, 'getSession']),
            new TwigFunction('flash', [$this, 'getMessages'])
        ];
    }

    public function getSession()
    {
        return (array)$this->container->get('session.service')->getSession();
    }

    /**
     * Returns Flash messages; If key is provided then returns messages
     * for that key.
     *
     * @param string $key
     *
     * @return array
     */
    public function getMessages($key = null)
    {
        if (null !== $key) {
            return $this->container->get('flash')->getMessage($key);
        }

        return $this->container->get('flash')->getMessages();
    }
}
