<?php
namespace Modules\Utils\Core;

use Modules\Utils\Core\BootMigrationConfiguration;
use Phinx\Migration\AbstractMigration;

class Migration extends AbstractMigration
{
    /** @var type manager */
    public $manager = null;
    /** @var type schema */
    public $schema = null;

    /**
     * init method
     * Contiene la implementación con eloquent para el sistema de migraciones
     */
    public function init()
    {
        list($this->manager, $this->schema) = BootMigrationConfiguration::boot();
    }
}
