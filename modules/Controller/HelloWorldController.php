<?php
namespace Modules\Controller;

use Illuminate\Database\Capsule\Manager as DB;
use Modules\Controller\BaseController;

class HelloWorldController extends BaseController
{
    public function sayHelloAction($request, $response, $args)
    {
        $view = $this->container->get('view');
        $log = $this->container->get('logger');
        $log->info("Slim-Skeleton '/' route");
        return $view->render($response, 'index.html.twig', $args);
        ;
    }
}
