<?php

use Modules\Utils\Core\TwigCoreExtensions;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Odan\Twig\TwigAssetsExtension;
use Slim\App;
use Slim\Http\Environment;
use Slim\Http\Uri;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use Twig\Extension\DebugExtension;

return function (App $app) {
    $container = $app->getContainer();

    // view renderer
    $container['view'] = function ($container) {
        $settings = $container->get('settings')['renderer'];

        $view = new Twig($settings['template_path'], [
            'cache' => $settings['cache_path'],
            'debug' => strtoupper($_ENV['ENV']) == 'PROD'?false:true
        ]);

        // Instantiate and add Slim specific extension
        $router = $container->get('router');
        $uri = Uri::createFromEnvironment(new Environment($_SERVER));
        $view->addExtension(new TwigExtension($router, $uri));
        $view->addExtension(new TwigAssetsExtension($view->getEnvironment(), $container->get('settings')['assets']));
        $view->addExtension(new TwigCoreExtensions($container));
        if (strtoupper($_ENV['ENV']) != 'PROD') {
            $view->addExtension(new DebugExtension());
        }
        return $view;
    };

    // monolog
    $container['logger'] = function ($container) {
        $settings = $container->get('settings')['logger'];
        $logger = new Logger($settings['name']);
        $logger->pushProcessor(new UidProcessor());
        $logger->pushHandler(new StreamHandler($settings['path'], $settings['level']));
        return $logger;
    };
};
