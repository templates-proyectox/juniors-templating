<?php
return [
    'settings' => [
        'displayErrorDetails' => strtoupper($_ENV['ENV']) == 'PROD'?false:true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'public_path' => __DIR__ . '/../public',
        'routerCacheFile' => strtoupper($_ENV['ENV']) == 'PROD'? __DIR__ . '/../storage/cache/routes.cache.php' : false,
        'determineRouteBeforeAppMiddleware' => true,

        
        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
            'cache_path' => strtoupper($_ENV['ENV']) == 'PROD'?__DIR__.'/../storage/cache/':false,
            'cache_enabled' => strtoupper($_ENV['ENV']) == 'PROD'? true : false
        ],

        // assets settings
        'assets' => [
            // Public assets cache directory
            'path' => __DIR__.'/../public/assets',
            'url_base_path' => 'assets/',
            // Cache settings
            'cache_enabled' => strtoupper($_ENV['ENV']) == 'PROD'? true : false,
            'cache_path' => strtoupper($_ENV['ENV']) == 'PROD'?__DIR__.'/../storage/cache/':false,
            'cache_name' => 'assets-cache',
            //  Should be set to 1 (enabled) in production
            'minify' => strtoupper($_ENV['ENV']) == 'PROD'? 1 : 0
        ],

        // Monolog settings
        'logger' => [
            'name' => $_ENV['PROJECT_NAME'],
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => constant("\Monolog\Logger::".strtoupper($_ENV['DEBUG_LEVEL'])),
        ]
    ],
];
