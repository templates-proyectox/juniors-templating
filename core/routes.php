<?php

use Slim\App;

return function (App $app) {
    $app->get('/[{name}]', "Modules\Controller\HelloWorldController:sayHelloAction");
};
