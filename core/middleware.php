<?php

use Slim\App;
use Tuupola\Middleware\CorsMiddleware;

return function (App $app) {
    $app->add(new CorsMiddleware([
        "origin" => ["*"],
        "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE"],
        "headers.allow" => ["content-type","x-requested-with","authorization"],
        "headers.expose" => ["access-control-allow-origin"],
        "credentials" => false,
        "cache" => 0,
    ]));
};
